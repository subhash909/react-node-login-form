import React, { Component } from 'react';
import LoginSignupPage from './LoginSignUp/loginsignup-page.jsx';
import HomePage from './HomePage/home_page.jsx';
import _ from 'lodash';
import logo from './logo.svg';
import './App.css';
import Container from 'react-bootstrap/Container'
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {

  constructor(props){
    super(props);
    _.bindAll(this, "fetch_roles")
    this.state = {
      is_logged_in: true
    };
  }

  componentWillMount() {
    this.fetch_roles();
  }

  fetch_roles(){
    var that = this;
    fetch("/session", {
      method: 'GET',
      dataType: 'json',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(response => { return response.json() })
    .then(response => {
      that.setState((prevState,props)=>{
        return {
          is_logged_in : response.session
        };
    });
    })
    .catch((error) => { 
    console.error(error);
    });
  }

  render() {
    var render_dom;
    if(this.state.is_logged_in===true) render_dom = <HomePage />;
    else if(this.state.is_logged_in===false) render_dom = <LoginSignupPage />
    else render_dom = <img src={logo} alt="logo" />
    return (
      <React.Fragment >
        <Container className="cont" style={{"margin-top": "50px"}}>
          {render_dom}
        </Container>
      </React.Fragment>
    );
  }
}

export default App;
