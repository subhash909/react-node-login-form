import React, { Component } from 'react';
import {Form, Button} from 'react-bootstrap';
import _ from 'lodash';

class SignupPage extends Component{

    constructor(props){
        super(props);
        _.bindAll(this, "handle_email_change", "handle_name_change", "handle_phone_no_change", "handle_pass_change", "handle_submit_click", "call_from_submit_api", "render");
        this.state = {
            username: "", useremail: "", phone_number: "", user_pass: "", error_message: ""
        };
    }

    handle_email_change(event) {
        this.setState({useremail: event.target.value});
    }

    handle_name_change(event) {
        this.setState({username: event.target.value});
    }

    handle_phone_no_change(event) {
        this.setState({phone_number: event.target.value});
    }

    handle_pass_change(event) {
        this.setState({user_pass: event.target.value});
    }

    handle_submit_click(event){
        var is_valid = true;
        var error_message = "";
        if(!this.state.useremail){
            is_valid = false;
            error_message = "Please provide a valid user email";
        }
        if(!this.state.username){
            is_valid = false;
            error_message = "Please provide a valid user name";
        }
        if(!this.state.phone_number){
            is_valid = false;
            error_message = "Please provide a valid phone number";
        }
        if(!this.state.user_pass){
            is_valid = false;
            error_message = "Please provide a valid Password.";
        }
        if(!is_valid){
            this.setState({error_message: error_message});
        }else{
            console.log("OK");
            this.call_from_submit_api();
        }

    }

    call_from_submit_api(){
        var that = this;
        fetch("/signup", {
          method: 'POST',
          body: JSON.stringify({u_name: this.state.username, e_id: this.state.useremail , p_no: this.state.phone_number, u_p: this.state.user_pass }),
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
        })
        .then(response => { return response.json() })
        .then(response => {
            var new_state = {};
            var error_message ="Some network error occurred, Please try again later.";
            if(response.STATUS && response.STATUS==="SUCCESS"){
                error_message = "Registered successfully.";
                new_state = { username: "", useremail: "", phone_number: "", user_pass: "", error_message: "" };
            }
            else if(response.TYPE === "DUPLICATE") error_message = "User already registered.";
            new_state.error_message = error_message;
            this.setState(new_state);
        })
        .catch((error) => { 
        console.error(error);
        });
      }

    render(){
        var error_dom;
        if(this.state.error_message) error_dom = <Form.Group controlId="fromusernumber" style={{"color": "red"}}><div>{this.state.error_message}</div></Form.Group>

        return (
            <React.Fragment>
                <Form controlId="signupform">
                    <Form.Group controlId="fromusername">
                        <Form.Label>User Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter User Name" value={this.state.username} onChange={this.handle_name_change} />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="fromuseremail">
                        <Form.Label>User Email</Form.Label>
                        <Form.Control type="mail" placeholder="Enter User Email"  value={this.state.useremail}  onChange={this.handle_email_change}/>
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="fromusernumber">
                        <Form.Label>Phone Number</Form.Label>
                        <Form.Control type="number" placeholder="Enter Phone Number"  value={this.state.phone_number}  onChange={this.handle_phone_no_change}/>
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="fromuserpass">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter Password"  value={this.state.user_pass}  onChange={this.handle_pass_change}/>
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>
                    {error_dom && error_dom}
                    <Button controlId="signupformsubmit" variant="primary" type="button" onClick={this.handle_submit_click}>
                        Submit
                    </Button>
                </Form>
            </React.Fragment>
        );
    }

}

export default SignupPage;