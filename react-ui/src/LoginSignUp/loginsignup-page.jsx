import React, { Component } from 'react';
import {Form, Button, Col, Row} from 'react-bootstrap';
import _ from 'lodash';
import SignupPage from './SignUpPage.jsx';
import LoginPage from './LoginPage.jsx';

class LoginSignupPage extends Component{

    constructor(props){
        super(props);
        this.state = {};
    }

    render(){
        var form_style = {
            "max-width": "800px",
            "margin": "auto auto",
            "border": "1px solid grey",
            "padding": "10px"
        };
        var error_dom;
        if(this.state.error_message) error_dom = <Form.Group controlId="fromusernumber" style={{"color": "red"}}><div>{this.state.error_message}</div></Form.Group>

        return (
            <React.Fragment>
                <div style={form_style}>
                    <Row>
                        <Col md="6">
                            <h4>Login</h4>
                            <hr/>
                            <LoginPage />
                        </Col>
                        <Col md="6"style={{"border-left": "1px solid grey"}} >
                            <h4>SignUp</h4>
                            <hr/>
                            <SignupPage />
                        </Col>
                    </Row>
                </div>
            </React.Fragment>
        );
    }

}

export default LoginSignupPage;