import React, { Component } from 'react';
import mapboxgl from 'mapbox-gl';
import MapboxDraw from 'mapbox-gl-draw';
import * as turf from '@turf/turf'
import {Col, Row, Button} from 'react-bootstrap';
import _ from 'lodash';

class HomePage extends Component{

    constructor(props){
        super(props);
        _.bindAll(this, "render", "updateArea");
        this.state = {};
    }

    componentDidMount() {
        mapboxgl.accessToken = 'pk.eyJ1Ijoic3ViaGFzaDkwOSIsImEiOiJjanNtMGUxZnEzNGd0NDVtbHF5ajVtODZ4In0.BkjNre0QH-meqWk3lfRqTA';
        var map = new mapboxgl.Map({
            container: 'world_map',
            zoom: 9,
            center: [137.9150899566626, 36.25956997955441],
            style: 'mapbox://styles/mapbox/satellite-v9'
        });

        var draw = new MapboxDraw({
            displayControlsDefault: false,
            controls: {
            polygon: true,
            trash: true
            }
        });

        map.addControl(draw);
         
        map.on('draw.create', this.updateArea);
        map.on('draw.delete', this.updateArea);
        map.on('draw.update', this.updateArea);

        this.draw = draw;
    }

    updateArea(event) {
        var data = this.draw.getAll();
        var answer = document.getElementById('calculated-area');
        if (data.features.length > 0) {
            var area = turf.area(data);
            var rounded_area = Math.round(area*100)/100;
            answer.innerHTML = '<p><strong>' + rounded_area + '</strong></p><p>square meters</p>';
        } else {
            answer.innerHTML = '';
            if (event.type !== 'draw.delete') alert("Use the draw tools to draw a polygon!");
        }
    }

    logout(event){
        fetch("/logout", {
            method: 'GET',
            dataType: 'json',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
        })
        .then(response => { return response.json() })
        .then(response => {
            if(response.STATUS && response.STATUS==="SUCCESS"){
                window.location.reload();
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render(){
        var map_style = { position: "absolute", top:"92px", bottom:"50px", width:"80%"};
        return (
            <React.Fragment>
                <Row>
                    <Col md="10">Welcome to home page</Col>
                    <Col md="1" className="pull-right">
                    <Button controlId="logoutsubmit" variant="primary" type="button" onClick={this.logout}>
                        Logout
                    </Button>
                    </Col>
                </Row>
                <div id="world_map" style={map_style}></div>
                <div class='calculation-box'>
                <p>Draw a polygon using the draw tools.</p>
                <div id='calculated-area'></div>
                </div>
            </React.Fragment>
        );
    }
}


export default HomePage;