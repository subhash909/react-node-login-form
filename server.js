var http = require('http');
var express = require('express');
var router = require('./models/Router');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();
app.use(express.static(path.join(__dirname, 'react-ui/build/')));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

router.register(app); //register the routes

http.createServer(app).listen(8080, function(){
    console.log("Server Started successfully on port 8080");
});
