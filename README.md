- Technology stacks:
	1. Node.js
	2. Express.js
	3. React.js
	4. MongoDB

- How to start node server.
	1. Move inside the root directory of project and run the following commands:
		- 'npm install' (It will install all node.js dependencies)
		- 'node server.js' (it will start the node.js server on port 8080)
		
- You can change the server port by modifying the server.js file.
- Please Make sure MongoDB is running properly on default port 27017 or you can change the port no. by modifying the ./models/DBConnection.js file.

