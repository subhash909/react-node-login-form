var signup_model = require('./SignUpModel');
var aut_model = require('./AuthModel');
var path = require('path');

var register = function(server){

    server.route('/signup')
    .post(signup_model.signup);

    server.route('/login')
    .post(aut_model.login);

    server.route('/logout')
    .get(aut_model.logout);

    server.route('/session')
    .get((req, res)=>{
        aut_model.check_token(req, res, function(req, res, u_email){
            if(u_email){
                res.send({"session": true});
            }else{
                res.send({"session": false});
            }
        });
    });

    server.route('/')
    .get((req, res)=>{
        res.sendFile(path.join(__dirname, '../react-ui/build/index.html'));
    })

}

var export_obj = {};
export_obj.register = register;

module.exports = export_obj;