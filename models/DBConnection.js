const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017';
const dbName = 'myproject';

var get_db_connection = function(callback){
  MongoClient.connect(url, callback);
}

var export_obj = {};
export_obj.get_db_connection = get_db_connection;

module.exports = export_obj;